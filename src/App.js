import React, {useEffect, useState} from 'react'
// import logo from './logo.svg';
import './App.css';

const useEventSource = (url) => {
  const [data, updateData] = useState(null);
  useEffect(() => {
      const source = new EventSource(url);

      source.onmessage = function logEvents(event) { 
          updateData(JSON.parse(event.data));     
      }
  }, [])

  return data;
}
function App() {
  const data = useEventSource('http://ec2-54-251-191-37.ap-southeast-1.compute.amazonaws.com:5001/service/subscribe/4975902051323093990');
  
  if (!data) {
    return <div />;
  }

  return (
    <div>
      <h3>POLDA KEPRI</h3>
      <div style={{width: '100%', height: '70vh', overflow: 'auto'}}>
        {JSON.stringify(data)}
      </div>
    </div>
  )
}

export default App;
